const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'member-gkn'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/member-gkn-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'member-gkn'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/member-gkn-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'member-gkn'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/member-gkn-production'
  }
};

module.exports = config[env];
