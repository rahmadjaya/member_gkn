var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    mongoose        = require('mongoose'),
    member   = mongoose.model('member')
var moment          = require('moment');
var cors            = require('cors');
module.exports = function (app) {
    app.use(cors());
    app.use('/bo/v1', router);
};
router.get('/members' , function(req,res,next){
    var data    =   member.find({});
    data.exec(function(err,result){
        res.json(result)
    })
})

router.post('/insertmember' , function(req , res , next){
    var data = member.findOne({username :   req.body.username});
    data.exec(function(err , result){
        if(result == null){
            var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format()
            var input = new member({
                        username    :   req.body.username,
                        first_name  :   req.body.first_name,
                        last_name   :   req.body.last_name,
                        jabatan     :   req.body.jabatan,
                        email       :   req.body.email,
                        gender      :   req.body.gender,
                        createdAt   :   today,
                        createdBy   :   req.body.username,
                        updatedAt   :   today,
                        updatedBy   :   req.body.username
                    })
            input.save(function(err , result){
                if(err){
                    res.json({hasil :   false , message :   "gagal menyimpan"});
                } else {
                    res.json({hasil    :   true ,  result  :   result  ,   message :   "berhasil"})
                }
            })
        } else {
            res.json({hasil :   false , message :   "user name sudah ada"})
        }
    })
})

router.put('/editmember/:username' , function(req,res,next){
    var data    =   member.findOne({username: req.params.username});
    data.exec(function(err,result){
        if(result != null){
            var today   =   moment(moment(new Date()), 'MM/DD/YYYY').format()
            var dataold = { _id         :   result._id};
            var datanew = { 
                            first_name  :   req.body.first_name,
                            last_name   :   req.body.last_name,
                            jabatan     :   req.body.jabatan,
                            email       :   req.body.email,
                            gender      :   req.body.gender,
                            updatedAt   :   today,
                            updatedBy   :   req.params.username
                        };
            var options = {};
            member.update(dataold , datanew , options , callback);
            function callback(err , resultcb){
                if (err) {
                    res.json({  
                                hasil   :   false ,
                                message :   "gagal Update"
                            })
                }   else    {
                    res.json({  
                                hasil   :   true ,
                                result  :   resultcb ,
                                message :   "Berhasil update"
                            })
                }
            }
        } else {
            res.json({
                hasil   :   false,
                message :   "Username tidak ada"
            })
        }
    })
})
router.delete('/delmember/:username' , async function (req,res,next){
    var data = await member.findOne({username:req.params.username});
    if (data) {
        await member.remove({username:data.username});
        res.json({
            hasil   :   true,
            message :   "data berhasil dihapus"
        })
    } else {
        res.json({
            hasil   :   false,
            message :   "Username tidak ada"
        })
    }
})
  