var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var userboSchema = new Schema({
  username    : {
    type      : String,
    required  : true
  },  
  first_name    : {
    type      : String,
    required  : true
  },
  last_name      : {
      type      : String,
      required  : true
  },
  jabatan     : {
      type      : String,
      required  : true
  },
  email     : {
      type      : String,
      required  : true
  },
  gender     : {
      type      : String,
      required  : true
  },
  createdAt   : {
    type    : String
  },
  createdBy   : {
      type    : String
  },
  updatedAt   : {
      type    : String
  },
  updatedBy   : {
      type    : String
  }
})

mongoose.model('member', userboSchema);